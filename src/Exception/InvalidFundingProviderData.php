<?php

namespace Drupal\funding\Exception;

/**
 * Used when YAML contains unvalid data.
 */
class InvalidFundingProviderData extends \Exception {}
