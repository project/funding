<?php

namespace Drupal\funding\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines funding_provider annotation object.
 *
 * @Annotation
 */
class FundingProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Whether the plugin should be enabled on production sites by default.
   *
   * Experimental plugins should use this to prevent site owners from enabling
   * risky plugins accidentally.
   *
   * @var bool
   */
  public bool $enabledByDefault = FALSE;

}
