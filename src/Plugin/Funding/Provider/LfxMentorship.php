<?php

namespace Drupal\funding\Plugin\Funding\Provider;

use Drupal\funding\Plugin\Funding\FundingProviderBase;

/**
 * Plugin implementation of the funding_provider.
 *
 * @FundingProvider(
 *   id = "community_bridge",
 *   label = @Translation("LFX Mentorship (previously Community Bridge)"),
 *   description = @Translation("Handles processing for the community_bridge funding namespace."),
 *   enabledByDefault = TRUE,
 * )
 */
class LfxMentorship extends FundingProviderBase {

  /**
   * {@inheritdoc}
   */
  public function build($data): array {
    if (is_string($data)) {
      return [
        '#theme' => 'funding_link',
        '#provider' => $this->id(),
        '#content' => $data,
        '#url' => 'https://funding.communitybridge.org/projects/' . $data,
      ];
    }

    return [];
  }

}
