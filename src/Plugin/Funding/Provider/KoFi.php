<?php

namespace Drupal\funding\Plugin\Funding\Provider;

use Drupal\funding\Plugin\Funding\FundingProviderBase;

/**
 * Plugin implementation of the funding_provider.
 *
 * @FundingProvider(
 *   id = "ko_fi",
 *   label = @Translation("Ko-Fi"),
 *   description = @Translation("Handles processing for the ko_fi funding namespace."),
 *   enabledByDefault = TRUE,
 * )
 */
class KoFi extends FundingProviderBase {

  /**
   * {@inheritdoc}
   */
  public function build($data): array {
    if (is_string($data)) {
      return [
        '#theme' => 'funding_link',
        '#provider' => $this->id(),
        '#content' => $data,
        '#url' => 'https://ko-fi.com/' . $data,
      ];
    }

    return [];
  }

}
