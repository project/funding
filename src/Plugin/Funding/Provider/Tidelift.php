<?php

namespace Drupal\funding\Plugin\Funding\Provider;

use Drupal\funding\Plugin\Funding\FundingProviderBase;

/**
 * Plugin implementation of the funding_provider.
 *
 * @FundingProvider(
 *   id = "tidelift",
 *   label = @Translation("Tidelift"),
 *   description = @Translation("Handles processing for the tidelift funding namespace."),
 *   enabledByDefault = TRUE,
 * )
 */
class Tidelift extends FundingProviderBase {

  /**
   * {@inheritdoc}
   */
  public function examples(): array {
    return [
      'tidelift: platform/package-name',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build($data): array {
    if (is_string($data)) {
      return [
        '#theme' => 'funding_link',
        '#provider' => $this->id(),
        '#content' => $data,
        '#url' => 'https://tidelift.com/funding/github/' . $data,
      ];
    }

    return [];
  }

}
